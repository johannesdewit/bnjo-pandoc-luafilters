
local function is_yaml_codeblock(block)
  for id, class in pairs(block.classes) do
    if class == 'yaml' then
      return true
    end
  end
  return false
end

function CodeBlock(block)
  if is_yaml_codeblock(block) then
    return {}
  end
end
