--[[Lua filter for Pandoc to restructure BPT markdown files to
BNJO part iv specification]]

--[[The file is read, the commentary blocks are written to another list than
the other parts of the file. Then the table index where the commentaries need to
be are ascertained and the lists are combined.
]]

-- Line included for backwards compatibility (pre Pandoc 2.6)
pandoc.utils = require 'pandoc.utils'


-- Local functions
-- Function to check whether string starts with another string
local function starts_with(str, start)
  if str:sub(1, #start) == start then
    return true
  end
  return false
end

-- Functions to check block functions.
local function block_is_header(el)
  if el.level then
    return true
  end
  return false
end

local function block_is_bibliography_header(el)
  if el.level == 3 and starts_with(el.identifier, 'bibliography') then
    return true
  end
  return false
end

local function block_is_fragment_commentary_header(el)
  if el.level == 4 and starts_with(el.identifier, 'commentary') then
    return true
  end
  return false
end

local function block_is_work_commentary_header(el)
  if el.level == 3 and starts_with(el.identifier, 'commentary') then
    return true
  end
  return false
end


-- Actual processing of the AST.
function Pandoc(doc)
  local w_comm = {}
  local f_comm = {}
  local textparts = {}
  local built_document = {}

-- Seperate blocktypes to restructure
  local a = false
  local b = false
  for _, block in pairs(doc.blocks) do
    if block_is_header(block) and
    not block_is_work_commentary_header(block) then
      a = false
    end
    if not block_is_fragment_commentary_header(block) and
    block_is_header(block) then
      b = false
    end
    if block_is_work_commentary_header(block) or a == true then
      table.insert(w_comm, block)
      a = true
    elseif block_is_fragment_commentary_header(block) or b == true then
      table.insert(f_comm, block)
      b = true
    else
      table.insert(textparts, block)
    end
    ::continue::
  end

-- Rebuilt and determine the new place of the commentaries
  local comm_pos
  local bibl_pos
  local c = false
  for index, block in ipairs(textparts) do
    if block_is_bibliography_header(block) and c == false then
      bibl_pos = index
      c = true
    end
    table.insert(built_document, block)
  end

-- Combine the commentaries into one table
  local commentaries = {}

  if w_comm[1] then
    for index, block in ipairs(w_comm) do
      table.insert(commentaries, block)
    end
  else
    -- Construct level 3 header if w_comm is empty.
    comm_header_block = pandoc.Header(3, "Commentary")
    table.insert(commentaries, comm_header_block)
  end

  for index, block in ipairs(f_comm) do
    table.insert(commentaries, block)
  end

-- Define position (table index) for the commentaries.
  comm_pos = bibl_pos

  for index, block in ipairs(built_document) do
    if index == comm_pos then
      for index, c in ipairs(commentaries) do
        table.insert(built_document, comm_pos, c)
        comm_pos = comm_pos + 1
      end
      comm_pos = 0
      goto continue
    end
    ::continue::
  end
  return pandoc.Pandoc(built_document, doc.meta)
end
