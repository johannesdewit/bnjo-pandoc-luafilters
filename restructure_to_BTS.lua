--[[Lua filter for Pandoc to restructure BPT markdown files to
BTS specification (documents/create_print_JO/typography-bnjo.pdf)]]

--[[The file is read, the commentary and bibliography blocks are written to
another list than the other parts of the file. Then the table indices where the
commentaries and bibliography need to be are ascertained and the lists are
combined.
]]

-- Line included for backwards compatibility (pre Pandoc 2.6)
pandoc.utils = require 'pandoc.utils'


-- Local functions
-- Function to check whether string starts with another string
local function starts_with(str, start)
  if str:sub(1, #start) == start then
    return true
  end
  return false
end

-- Functions to check block functions.
local function block_is_header(el)
  if el.level then
    return true
  end
  return false
end

local function block_is_textpart_header(el)
  if el.level == 3 and starts_with(el.identifier, 'textpart') then
    return true
  end
  return false
end

local function block_is_bibliography_header(el)
  if el.level == 3 and starts_with(el.identifier, 'bibliography') then
    return true
  end
  return false
end

local function block_is_notes_header(el)
  if el.level == 3 and starts_with(el.identifier, 'notes') then
    return true
  end
  return false
end

local function block_is_fragment_commentary_header(el)
  if el.level == 4 and starts_with(el.identifier, 'commentary') then
    return true
  end
  return false
end

local function block_is_work_commentary_header(el)
  if el.level == 3 and starts_with(el.identifier, 'commentary') then
    return true
  end
  return false
end


-- Actual processing of the AST.
function Pandoc(doc)
  local w_comm = {}
  local f_comm = {}
  local commentaries = {}
  local textparts = {}
  local bibl = {}
  local built_document = {}

-- Seperate blocktypes to restructure
  local a = false
  local b = false
  local c = false
  for _, block in pairs(doc.blocks) do
    -- Bibliography can have sub headers
    if block_is_header(block) and
    not block_is_work_commentary_header(block) then
      a = false
    end
    if block_is_header(block) and
    not block_is_fragment_commentary_header(block) then
      b = false
    end
    if block_is_header(block) and
    block_is_notes_header(block) then
      c = false
      table.insert(textparts, block)
      goto continue
    end
    if block_is_work_commentary_header(block) or a == true then
      table.insert(w_comm, block)
      a = true
    elseif block_is_fragment_commentary_header(block) or b == true then
      table.insert(f_comm, block)
      b = true
    elseif block_is_bibliography_header(block) or c == true then
      table.insert(bibl, block)
      c = true
    else
      table.insert(textparts, block)
    end
    ::continue::
  end

-- Combine the commentaries into one table
  if w_comm[1] then
    for index, block in ipairs(w_comm) do
      table.insert(commentaries, block)
    end
  else
    -- Construct level 3 header if w_comm is empty.
    comm_header_block = pandoc.Header(3, "Commentary")
    table.insert(commentaries, comm_header_block)
  end

  for index, block in ipairs(f_comm) do
    table.insert(commentaries, block)
  end

-- Rebuilt and determine the new place of the commentaries
  local d = false
  local textpart_pos
  local insert_pos
  for index, block in ipairs(textparts) do
    if block_is_textpart_header(block) and d == false then
      textpart_pos = index
      d = true
    end
    table.insert(built_document, block)
  end

  insert_pos = textpart_pos

-- Define position (table index) for the commentaries.
  for index, block in ipairs(built_document) do
    if index == insert_pos then
      for index, c in ipairs(commentaries) do
        table.insert(built_document, insert_pos, c)
        insert_pos = insert_pos + 1
      end
      for index, b in ipairs(bibl) do
        table.insert(built_document, insert_pos, b)
        insert_pos = insert_pos + 1
      end
      insert_pos = 0
      goto continue
    end
    ::continue::
  end
  return pandoc.Pandoc(built_document, doc.meta)
end
