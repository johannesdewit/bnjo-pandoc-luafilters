# Pandoc BNJO Lua Filters

## Introduction

When trying to create good looking PDF-files from [Brill Plain Text (BPT)](https://brillpublishers.gitlab.io/documentation-brill-plain-text/) used in the BNJO-project by means of [Pandoc](https://pandoc.org), it seemed that the order of the file cannot easily be altered in a normal conversion. It seems that so called ‘Lua Filters’ might provide an option to target the individual text parts and codeblocks individually. When combined with [custom templates](https://gitlab.com/johannesdewit/bnjo-pandoc-textemplates), these filters provide a way to create pretty PDF-files from the bpt files. 

## Details

Pandoc uses an ‘abstract syntax tree’ in order to render files between parsing and writing. This AST can be modified by filters, which are written in the Lua language.

See also: [Pandoc’s website on Lua filters](https://pandoc.org/lua-filters.html).

## Use

There are two ways of using the filters. Either installed as a global filter, or by specifying the full file path.

When copied to `$PANDOC_DIR/filters/` (where `$PANDOC_DIR` is the output of `pandoc -v`) these filters can be applied by using the flag `--lua-filter=filename.lua` or `-L filename.lua` when running the conversion.

Filters can also be used by specifying the whole file path.

## The different filters

`remove_codeblocks.lua`
Removes all codeblocks from the BPT-file.

`remove_codeblock_yaml_only.lua`
Removes all *yaml* codeblocks from the BPT-file, thus leaving the bibliography intact.

`restructure_to_BTS.lua`
Restructures the BPT-file to match the order defined in the BTS-guide (documents/create_print_JO/typography-bnjo.pdf).

`restructure_to_BNJOiv.lua`
Restructure the BPT-file to match the order defined for Part IV of BNJO.
